<?php

/**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage Eire Workforce Solutions
 * @since Eire Workforce Solutions 1.0
 */


get_header(); ?>

<div id="main" class="contact">
           
       <header class="title">

            <h1 class="full"><?php the_title(); ?>
            <span class="icon-contact-us"></span>
            </h1>

       </header>
	  <div class="overlay" onClick="style.pointerEvents='none'"></div>
        <iframe
          width="100%"
          height="500"
          frameborder="0" style="border:0"
          src="https://www.google.com/maps/embed/v1/place?q=6th+Floor,+First+Central+200,+2+Lakeside+Drive,+Park+Royal,+London+NW10+7FQ&key=AIzaSyCJEdJZ7LCezEeEk_qKg4kVrW5D78qgcg8">
        </iframe>
          
        <div class="contact-phone dark">
            
            <div class="full">
                
                <h2 class="left-reveal"><?php the_field('contact_primary_phone_number'); ?></h2>
                
                <h3 class="left-reveal"><?php the_field('contact_primary_phone_number_subtitle'); ?></h3>
                
                <span class="icon-phone right-reveal"></span>
                
            </div>
            
        </div>  
                
        <div class="contact-email light">
            
            <div class="full">
                
                <h2 class="left-reveal"><?php the_field('contact_primary_email_address'); ?></h2>
                
                <h3 class="left-reveal"><?php the_field('contact_primary_email_address_subtitle'); ?></h3>
                
                <span class="icon-envelope right-reveal"></span>
                
            </div>
            
        </div>  
                
    <?php if (have_rows('contact_offices')) : ?>
    
    <div class="full flex-container offices">
    
    	<?php while( have_rows('contact_offices') ): the_row(); ?>
            
           <div class="material third light">   
            
                <h3 class="left-reveal"><?php the_sub_field('contact_office_name'); ?></h3>
                
                <?php
			   		
			   		if (have_rows('contact_office_address')) :
			   		
			   		while (have_rows('contact_office_address')) : the_row();
			   	
			   	?>
                
                <span class="address-line left-reveal"><?php the_sub_field('contact_office_address_line'); ?></span>
                
                <?php endwhile; endif; // haverows address lines ?>
                
                <h4 class="left-reveal"><?php the_sub_field('contact_office_number'); ?></h4>
                <a class="button wide-button"
                href="tel:<?php the_sub_field('contact_office_number'); ?>">
                Call <?php the_sub_field('contact_office_name'); ?></a>
                
            </div>
          
         <?php endwhile; // haverows offices ?>       
                
	</div>
                           
    <?php endif; // endif haverows offices ?>
                           
    <div class="send-message gradient">
    	
    	<div class="full">
    		
    		<h2 class="left-reveal"><?php the_field('contact_message_title'); ?></h2>
    		<h3 class="left-reveal"><?php the_field('contact_message_subtitle'); ?></h3>
    		
    		<span class="icon-send fade-reveal"></span>
    		
    	</div>
    	
    </div>
    
    <div class="full">
        
        <div class="material form light">

          <?php gravity_form( 1, false, false, false, '', true ); ?>

        </div>
        
    </div>
         
    <div class="social">
    	
		<div class="facebook half">

			<div class="container">

				<h2 class="left-reveal"><?php the_field('contact_facebook_title'); ?></h2>

				<a class="button left-reveal" href="<?php the_field('contact_facebook_page'); ?>">
				<?php the_field('contact_facebook_button_text'); ?>
				</a>

				<span class="icon-facebook left-reveal"></span>

			</div>

		</div>

		<div class="linkedin half">

			<div class="container">

				<h2 class="right-reveal"><?php the_field('contact_linkedin_title'); ?></h2>

				<a class="button right-reveal" href="<?php the_field('contact_linkedin_page'); ?>">
				<?php the_field('contact_linkedin_button_text'); ?>
				</a>

				<span class="icon-linkedin right-reveal"></span>

			</div>

		</div>
    	
    </div>
                          
                           
                            
</div>

<?php get_footer(); ?>