$(document).ready(function() {
  function wayPointHandler(elem, direction) {
    if (direction === "down") {
      $(elem.element).addClass("in-view");
    } else if (direction === "up") {
      $(elem.element).removeClass("in-view");
    } else {
      $(elem.element).addClass("in-view");
    }
  }

  function wayPointBinder() {
    $(".icon-car").waypoint({
      handler: function() {
        wayPointHandler(this);
      },
      offset: "90%"
    });

    $(".sector").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $(".offices .third").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $(".fade-reveal").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $(".left-reveal").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $(".top-reveal").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $(".right-reveal").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $(".bottom-reveal").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $("footer .menu-footer-menu-container ul li").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });

    $(".full .form").waypoint({
      handler: function(direction) {
        wayPointHandler(this, direction);
      },
      offset: "90%"
    });
  }

  wayPointBinder();

  function staggerAnimations(selector) {
    selector.each(function(index) {
      $(this)
        .eq(index)
        .css({ transitionDelay: index * 200 + "ms !important" });
    });
  }

  staggerAnimations($("footer .menu-footer-menu-container ul li"));

  var lastScroll = $(window).scrollTop();

  function scroller() {
    var scroll = $(window).scrollTop();

    /* Going down */

    if (scroll > lastScroll && scroll > 120) {
      if (!$("body").hasClass("mobile")) {
        $("body").addClass("slim");
      } else if (!$("body").hasClass("open")) {
        $("body").addClass("nav-hidden");
      }
    } else {
      /* Going up */

      if (!$("body").hasClass("mobile")) {
        $("body").removeClass("slim");
      } else {
        $("body").removeClass("nav-hidden");
      }
    }

    lastScroll = scroll;
  }

  $(window).scroll(function() {
    scroller();
  });

  function checkMobile() {
    if ($(window).width() <= 1200) {
      $("body").addClass("mobile");
    } else {
      $("body").removeClass("mobile nav-hidden");
    }
  }

  checkMobile();

  $(window).resize(function() {
    checkMobile();
  });

  function bindSelects() {
    $("select").each(function() {
      $(this).change(function() {
        if ($(this).val() !== "") {
          $(this).addClass("selected");
        } else {
          $(this).removeClass("selected");
        }
      });
    });
  }

  bindSelects();

  $('input[type="button"]').each(function() {
    $(this).click(function() {
      bindSelects();
    });
  });

  $("#handle").click(function() {
    if ($("body").hasClass("mobile") && $("body").hasClass("closed")) {
      $("body")
        .addClass("open")
        .removeClass("closed");
      $(".hamburger").addClass("is-active");
    } else if ($("body").hasClass("open")) {
      $("body")
        .addClass("closed")
        .removeClass("open");
      $(".hamburger").removeClass("is-active");
    }
  });

  /**
   * Splash modal
   */
  if ($(".splash").length > 0) {
    $("body, html").addClass("body--splash");
    $(".splash .material").on("click", function() {
      $(".splash").addClass("splash--hidden");
      $("body, html").removeClass("body--splash");
    });
  }

  $(window).on("load", function() {
    $(".loader").addClass("loader--loaded");
  });
});
