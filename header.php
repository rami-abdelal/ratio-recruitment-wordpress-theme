<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo content_url(); ?>/uploads/2018/07/favicon-512x512.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#ffffff">
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.waypoints.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
    <?php 
    gravity_form_enqueue_scripts( 1, true );
    gravity_form_enqueue_scripts( 2, true );
    gravity_form_enqueue_scripts( 3, true );
    gravity_form_enqueue_scripts( 4, true );
    wp_head(); 
    ?>
</head>

<body class="closed">
           
        <nav>
			<div class="full">
				<a class="logo"
				href="<?php echo home_url(); ?>">
				<img src="<?php echo content_url() . "/uploads/2018/07/logo-12.png" ?>" alt="Ratio Recruitment Logo">
                <h6>Ratio Recruitment</h6></a>
                
				<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
				
				<div id="handle">

					<button class="hamburger hamburger--spring" type="button">
						<h6>Menu</h6>
					  <span class="hamburger-box">

						<span class="hamburger-inner"></span>

					  </span>

					</button>

				</div>

			</div>          
        </nav>
        
        <aside>
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
        </aside>

