<?php

/**
 * Template Name: About
 *
 * @package WordPress
 * @subpackage Eire Workforce Solutions
 * @since Eire Workforce Solutions 1.0
 */


 get_header(); ?>

<div id="main" class="about">
   

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title gradient">

            <h1 class="full"><?php the_title(); ?>
            <span class="icon-about"></span>
            </h1>

       </header>
       
		<div class="full">
		
			<div class="light material-full material about-introduction">

				 <h2><?php the_field('about_introductory_title'); ?></h2>

				 <p><?php the_field('about_introductory_paragraph'); ?></p>

				<div class="half-image" style="background-image: url(<?php the_field('about_introductory_image'); ?>);"></div>

			</div>

		</div>
           
        <div class="international-image large" style="background-image:url(<?php the_field('about_international_image'); ?>)">
            
            <div class="gradient screen">
                
                <div class="full">
                    
                    <h2 class="left-reveal"><?php the_field('about_international_title'); ?></h2>                    
                    <h3 class="left-reveal"><?php the_field('about_international_subtitle'); ?></h3>
                    
                    
                </div>
                
            </div>
            
        </div>
          
          <div class="wide-article light">
              
              <div class="full">
                  
                  <h4 class="left-reveal"><?php the_field('about_international_article_title'); ?></h4>             <p class="left-reveal"><?php the_field('about_international_article_paragraph'); ?></p>
                  
              </div>
              
          </div>
          
          <div class="team gradient large">
              
              <div class="full">
                  
                  <h2 class="left-reveal"><?php the_field('about_team_title'); ?></h2>
                  <h3 class="left-reveal"><?php the_field('about_team_subtitle'); ?></h3>
                  
                  <span class="bottom-reveal icon-team"></span>
                  
              </div>
              
          </div>
           
          <div class="wide-article light">
              
              <div class="full">
                  
                  <h4 class="left-reveal"><?php the_field('about_team_article_title'); ?></h4>
                  <p class="left-reveal"><?php the_field('about_team_article_paragraph'); ?></p>
                  
              </div>
              
          </div>
           
           
    <?php endwhile; endif; ?>
            
</div>

<?php get_footer(); ?>