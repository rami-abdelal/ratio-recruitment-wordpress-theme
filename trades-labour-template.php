<?php

/**
 * Template Name: Trades & Labour
 *
 * @package WordPress
 * @subpackage Eire Workforce Solutions
 * @since Eire Workforce Solutions 1.0
 */


 get_header(); ?>

<div id="main" class="trades-labour">
   

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title gradient">

            <h1 class="full"><?php the_title(); ?>
            <span class="icon-trades-labour"></span>
            </h1>

       </header>
       
    <div class="material light">
        
        <div class="full">
            
            <h3>Are you a tradesman or labourer?</h3> 
            <p>&nbsp;</p>
            <h6>Join our workforce today.</h6>
            <p>&nbsp;</p>
            <p>Just fill out the form below to send us your application and we'll get back to you within 2 working days.</p>
            </div>
        
    </div>
    
    <div class="full">

        <div class="material lght form">

          <?php gravity_form( 4, false, false, false, '', true ); ?>

        </div>

    </div>
           
           
    <?php endwhile; endif; ?>
            
</div>

<?php get_footer(); ?>