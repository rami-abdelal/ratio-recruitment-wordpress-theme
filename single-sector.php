<?php

/*
Template Name: Single sector
Template Post Type: sectors
*/


get_header(); ?>

<div id="main" class="sector-single">
  
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

   <header class="title dark" style="background-image:url(<?php the_field('sector_photo'); ?>)">
	<div class="scrim">
        <h1 class="full"><?php the_title(); ?>
        </h1>
	</div>
   </header>
   
       
        <div class="wide-article light">
           
            <div class="full">
            	
				<h4><?php the_field('sector_subtitle'); ?></h4>

				<?php the_content(); ?>
            	
            </div>
            
        </div>
              	
   <div class="bar gradient more-sectors">
        
        <div class="full">
            
            <h3>Looking for a different Sector? </h3>
            
            <a class="button" href="<?php get_site_url(); ?>/sectors/">
            <span class="icon-sectors"></span>
            Sectors
            </a>
            
        </div>
        
    </div>              

               	
   </div>

<?php endwhile; endif; ?>


<?php get_footer(); ?>