<footer>
    <div id="pages-wrap">
        <div id="pages" class="full">
            <h3>Pages</h3>
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>

            <span class="icon-logo"></span>
        </div>
    </div>
    <div class="dark credentials bar">
       
        <div class="full">
            <p>Ratio Recruitment &copy; <?php echo date("Y"); ?>. All Rights Reserved.</p>
            <a href="modern-slavery-and-human-trafficking-statement">Modern Slavery Statement</a>
        </div>
        
    </div>
</footer>
</div>
</div>
<div class="loader">
    <div class="full"><img src="<?= get_template_directory_uri() . '/img/favicon-512x512.png' ?>" alt="Ratio Recruitment Loading Icon" width="512" height="512">
</div>
</body>
</html>
