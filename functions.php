<?php

// Add the filter to manage the p tags
// add_filter( 'the_content', 'wti_remove_autop_for_image', 0 );

function disable_admin_bar(){ return false; }
add_filter( 'show_admin_bar' , 'disable_admin_bar');

function wti_remove_autop_for_image( $content )
{
     global $post;

          remove_filter('the_content', 'wpautop');

     return $content;
}

function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );


function create_post_type() {
    
  register_post_type( 'sectors',
    array(
      'labels' => array(
        'name' => __( 'Sectors' ),
        'singular_name' => __( 'Sector' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'sectors'),
	  'menu_icon' => 'dashicons-chart-pie',
    )
  );
    
}

add_action( 'init', 'create_post_type' );

function add_favicon() {

  $favicon_url = content_url() . "/uploads/2018/07/favicon-512x512.png";
  echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';

}

add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

?>