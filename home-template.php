<?php

/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Eire_Workforce_Solutions
 * @since Eire Workforce Solutions 1.0
 */


get_header(); ?>

<div id="main" class="home">
   
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <div id="hero-wrapper dark">
        
        <div id="hero-image" style="background-image: url(<?php the_field('hero_photo'); ?>)">
            
            <div class="gradient screen">
               
                <div class="full">
                    
                    <h1><?php the_field('hero_title'); ?></h1>

                    <?php if (get_field('hero_subtitle')) : ?>

                    <h2><?php the_field('hero_subtitle'); ?></h2>

                    <?php endif;

                    /* Build call to action button */

                    $buttonType = get_field('hero_call_to_action_button_type');

                    if ($buttonType != 'none') :

                        echo '<a class="button" href="';

                        if ($buttonType == 'email') {

                            echo "mailto:";

                            the_field('hero_call_to_action_button_email_address');

                            echo '">';

                        } elseif ($buttonType == 'tel') {

                            echo "tel:";

                            the_field('hero_call_to_action_button_phone_number');

                            echo '">';

                        } elseif ($buttonType == 'page' && get_field('hero_call_to_action_button_page_link')) {

                            the_field('hero_call_to_action_button_page_link');

                            echo '">';

                        }
                        
                        the_field('hero_call_to_action_button_text');

                        echo '</a>';

                    endif; // button

                    ?>

                </div>
                
            </div>
            
        </div> <!-- End hero -->
        
    </div>
    
    <?php if (have_rows('form_adverts')) : ?>
    
    <div class="full flex-container form-adverts">
    
    	<?php $counter = 1;
		while( have_rows('form_adverts') ): the_row(); ?>
    	
    	<div class="half material light form-advert <?php echo ($counter%2==0 ? 'right-reveal' : 'left-reveal'); ?>">
    		
    		<h2><?php the_sub_field('form_advert_title'); ?></h2>
    		
    		<h3><?php the_sub_field('form_advert_subtitle'); ?></h3>
    		
			<span class="<?php the_sub_field('form_advert_icon'); ?>"></span>
   		
			<a class="button wide-button" href="<?php the_sub_field('form_advert_page_link'); ?>">
				<?php the_sub_field('form_advert_button_text'); ?>
			</a>
    		
    	</div>
    	
    	<?php 	$counter++;
				endwhile; /* form advert */ ?>
    	
    <?php endif; /* endif have rows form adverts */ ?>
    	
    	
    </div> <!-- form adverts container -->
    
    <div class="sectors-advert dark fade-reveal">
        
        <div class="full">
            
            <h2><?php the_field('sectors_advert_title'); ?></h2>
            <h3><?php the_field('sectors_advert_subtitle'); ?></h3>
            
            <span class="icon-sectors"></span>
            
        </div>
        
    </div>
    
    <div class="full flex-container sectors">
        
        <?php 
        
        $args = array( 'post_type' => 'sectors', 'posts_per_page' => 3, orderby => 'menu_order');
        
        $loop = new WP_Query( $args );
    
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

        <div class="sector third material dark" style="background-image:url(<?php the_field('sector_photo'); ?>);"> 
          
          <div class="scrim">
              
               <a href="<?php the_permalink();?>">

                    <?php the_title(); ?>

                </a>

          </div>
           
        </div>
    
        <?php endwhile; wp_reset_query(); /* Sector loop */ ?>
        
    </div>
             
    
              
    <div class="bar dark more-sectors">
        
        <div class="full">
            
            <h3><?php the_field('sectors_advert_more_message'); ?></h3>
            
            <a class="button" href="<?php get_site_url(); ?>/sectors/">
            <span class="icon-sectors"></span>
            Sectors
            </a>
            
        </div>
        
    </div>              
    
    <div class="services-advert dark large" style="background-image:url(<?php the_field('services_advert_image'); ?>)">
        
    <div class="screen">
            
            <div class="full">

                <h2><?php the_field('services_advert_title'); ?></h2>

                <h3><?php the_field('services_advert_subtitle'); ?></h3>

                <p><?php the_field('services_advert_paragraph'); ?></p>

                <a class="button" href="<?php the_field('services_advert_button_page_link'); ?>">
                   <span class="icon-services"></span>
                    <?php the_field('services_advert_button_text'); ?>
                </a>

            </div>

        </div>
        
    </div>
              
    <div class="careers-advert light large">
       
        <div class="full">
            
            <h2><?php the_field('careers_at_ratio_advert_title'); ?></h2>
            <h3><?php the_field('careers_at_ratio_advert_subtitle'); ?></h3>
            <p><?php the_field('careers_at_ratio_advert_paragraph'); ?></p>
            
            <span class="icon-car"></span>
            
            <a class="button" href="<?php the_field('careers_at_ratio_button_page_link'); ?>">
            
            <span class="icon-careers"></span>
            
            <?php the_field('careers_at_ratio_advert_button_text'); ?>
            
            </a>
            
        </div>
        
    </div>
               
    <?php endwhile; else: ?>
    <?php endif; ?>
        
</div>

<div class="splash">
    <video poster="<?= get_template_directory_uri() . '/img/ratio-recruitment-splash-poster.jpg'?>" playsinline loop autoplay muted>
        <source src="<?= get_template_directory_uri() . '/img/ratio-recruitment-splash.mp4'?>">
    </video>
    <div class="splash__overlay"></div>
    <div class="full flex-container">
        <a href="javascript:void(0)" class="half material light ratio-digital">
            <img src="<?= get_template_directory_uri() . '/img/ratio-digital-logo.png'?>" alt="Ratio Digital Recruitment Logo">
        </a>
        <a href="javascript:void(0)" class="half material light ratio-construction">
            <img src="<?= get_template_directory_uri() . '/img/ratio-construction-logo.png'?>" alt="Ratio Construction Recruitment Logo">
        </a>
    </div>
</div>

<?php get_footer(); ?>