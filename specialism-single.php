<?php 

/*
Template Name: Single specialism
Template Post Type: specialisms
*/

get_header(); ?>

<div id="main">
  
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   
   <header class="title">
       
        <h1 class="full"><?php the_title(); ?></h1>
        
   </header>
        
     <?php if ( have_rows('featured_jobs')): ?>
      
   <div class="half-container full">

      <?php  
       
       while ( have_rows('featured_jobs')) : the_row();
              
       ?>
      
       <div class="material job half">
          
           <h2 class="text-gradient"><?php the_sub_field('title'); ?></h2>
           <table>
               <tr>
                   <td><strong>Location:</strong> <?php the_sub_field('location'); ?></td>
                   <td><strong>Rate:</strong> <?php the_sub_field('rate'); ?></td>
               </tr>
               <tr>
                   <td><strong>Date:</strong> <?php the_sub_field('date'); ?></td>
                   <td><strong>Type:</strong> <?php the_sub_field('type'); ?></td>
               </tr>
           </table>
           <a href="<?php the_sub_field('link'); ?>" class="button">View details</a>
           
       </div>
       
       <?php endwhile; ?>
       
   </div>
   
   <div class="find-more-jobs dark bar">
       
       <div class="full">
           
           <p>Find more jobs like these</p>
           
           <a class="button" href="<?php echo get_site_url(); ?>/jobs">Jobs</a>
           
       </div>
       
   </div>
   
       <?php
    
       else :
       
       // no featured jobs
       
       endif;
       
    ?>
   
   <div id="specialism-content" class="material wide">
       
        <div class="full">

            <h1 class="page-title text-gradient"><?php the_title(); ?></h1>
            
            <div class="content">
                
                <?php
                    echo '<span class="';

                    the_field('icon');

                    echo '"></span>';
                ?>

                <p><?php the_field('intro');?></p>

                
                <?php if ( have_rows('list')): ?>
                
                <ul>
                <?php  
       
                   while ( have_rows('list')) : the_row();

                   
                ?>
      
               <li><?php the_sub_field('list_row'); ?></li>
               
               <?php endwhile; ?>
               
                </ul>
                
                <?php
    
                   else :

                   // no list rows

                   endif;

                ?>

                
                 <?php the_content(__('(more...)')); ?>

               <a class="button" href="<?php echo get_site_url(); ?>/jobs">Search for these jobs</a>
               
                <?php endwhile; else: ?>

                <?php endif; ?>

            
            </div>

        </div>
        
    </div>
    
   <div class="see-more-specialisms dark bar">
       
       <div class="full">
           
           <p>Looking for a different specialism?</p>
           
           <a class="button" href="<?php echo get_site_url(); ?>/specialisms">Specialisms</a>
           
       </div>
       
   </div>
    
</div>

<?php get_footer(); ?>