<?php

/**
 * Template Name: Careers at EWS
 *
 * @package WordPress
 * @subpackage Eire Workforce Solutions
 * @since Eire Workforce Solutions 1.0
 */


 get_header(); ?>

<div id="main" class="careers">
   

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title gradient">

            <h1 class="full"><?php the_title(); ?>
            <span class="icon-careers"></span>
            </h1>

       </header>
       
    <div class="wide-article light">
        
        <div class="full">
            
            <h2>Looking for a career in recruitment?</h2> 
            <p>&nbsp;</p>
            <h6>Join us here at Ratio Recruitment by filling out the form below to send in your application.</h6>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>Whether you’re new to recruitment or you’ve got some experience under your belt, there are some exciting advantages to working with us:</p>
            <p>&nbsp;</p>
            <ul class="bullet-list">
            <li>Competitive Salary</li>
            <li>Uncapped Commission</li>
            <li>Company Laptop</li>
            <li>Company Mobile</li>
            <li>Fruit &amp; Cakes provided at the office</li>
            <li>Monthly Billers Lunch</li>
            <li>25 Days Holiday with Buy options</li>
            <li>Paid Travel to work</li>
            <li>Cycle to Work Scheme</li>
            <li>Corporate Rates at the Gym</li>
            <li>Discounted Supermarket Shopping</li>
            <li>Company Car Allowance Available</li>
            <li>Weekly Pay Option Available</li>
            </ul>
                      
        </div>
        
    </div>

    <div class="full">

        <div class="material form">

          <?php gravity_form( 3, false, false, false, '', true ); ?>

        </div>

    </div>
           
           
    <?php endwhile; endif; ?>
            
</div>

<?php get_footer(); ?>