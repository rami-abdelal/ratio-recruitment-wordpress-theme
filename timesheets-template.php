<?php

/**
 * Template Name: Timesheets
 *
 * @package WordPress
 * @subpackage Eire Workforce Solutions
 * @since Eire Workforce Solutions 1.0
 */


get_header(); ?>

<div id="main">

        <header class="title gradient" style="display:none">

            <h1 class="full"><?php the_title(); ?>
            <span class="icon-contact-us"></span>
            </h1>

       </header>
       
       <div class="send-timesheet gradient">
    	
    	<div class="full">
    		
    		<h2 class="left-reveal in-view"><?php the_field('timesheet_header_title'); ?></h2>
    		<h3 class="left-reveal in-view"><?php the_field('timesheet_header_subtitle'); ?></h3>
    		
    		<span class="icon-timesheet fade-reveal in-view"></span>
    		
    	</div>
    	
    </div>
    
    <?php
        
        $formInstructions = get_field('additional_form_instructions');
    
        if ($formInstructions) {
            
            echo '<div class="material light"><div class="full">' . $formInstructions . '</div></div>' ;
    
        }
    
    ?>
    
    <div class="full">
        
        <div class="material light form">

          <?php gravity_form( 5, false, false, false, '', true ); ?>

        </div>
        
    </div>
    
    <div class="bar gradient download-timesheet">
        
        <div class="full">
            
            <h3>Download the timesheet PDF</h3>
            
            <a class="button" href="http://eireworkforce.co.uk/wp-content/uploads/2017/12/Eire-Workforce-Timesheet-Blank.pdf">
            <span class="icon-download"></span>
            Download
            </a>
            
        </div>
        
    </div>                                               
                           
                            
</div>

<?php get_footer(); ?>