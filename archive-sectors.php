<?php

/**
 * Template Name: Sectors Archive
 *
 * @package WordPress
 * @subpackage Ratio_Digital_Recruitment
 * @since Ratio Digital Recruitment 1.0
 */


get_header(); ?>

<div id="main" class="sectors-archive">
     
   <header class="title gradient">

        <h1 class="full">Sectors
        <span class="icon-sectors"></span>
        </h1>

   </header>
   
   <div class="full flex-container">
   	
    <?php 
        
        $args = array( 'post_type' => 'sectors', 'posts_per_page' => 10, orderby => 'menu_order');
        
        $loop = new WP_Query( $args );
    
        while ( $loop->have_posts() ) : $loop->the_post();

        ?>
        
        <div class="material light sector">
            
            <h2><?php the_title(); ?></h2>
            
            <h3><?php the_field('sector_subtitle'); ?></h3>
            
            <div class="sector-photo half-image" style="background-image:url(<?php the_field('sector_photo'); ?>);">
            </div>
            
            <a class="button wide-button" href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
            </a>
            
        </div>
    
    <?php endwhile; // Sectors Loop ?>
           	
   </div>

</div>


<?php get_footer(); ?>