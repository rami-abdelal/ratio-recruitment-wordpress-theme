<?php

/**
 * Template Name: Clients
 *
 * @package WordPress
 * @subpackage Eire Workforce Solutions
 * @since Eire Workforce Solutions 1.0
 */


 get_header(); ?>

<div id="main" class="clients">
   

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title gradient">

            <h1 class="full"><?php the_title(); ?>
            <span class="icon-clients"></span>
            </h1>

       </header>
       
    <div class="material light">
        
        <div class="full">
            
            <h3>Need boots on the ground?</h3> 
            <p>&nbsp;</p>
            <h6>Our workforce is ready whenever you are.</h6>
            <p>&nbsp;</p>
            <p>Just fill out the form below to send us your request and we'll respond by the next working day.</p>
            <p>Alternatively, if you need to speak to someone right away, call our London office now to get in touch with one of our agents.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <a class="button" href="tel:0207 663 8016">
            <span class="icon-phone"></span>
				0207 663 8016           
            </a>
            </div>
        
    </div>

    <div class="full">

        <div class="material light form">

          <?php gravity_form( 2, false, false, false, '', true ); ?>

        </div>

    </div>
           

           
    <?php endwhile; endif; ?>
            
</div>

<?php get_footer(); ?>