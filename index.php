<?php get_header(); ?>

<div id="main" class="default-page">
   
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title">
            <div class="full">
                <h1><?php the_title(); ?></h1>
            </div>
       </header>
          
        <div class="material full">
        <?php the_content(__('(more...)')); ?>
        <?php endwhile; else: ?>
        <p><?php _e('Sorry, index php no posts matched your criteria.'); ?></p><?php endif; ?>
        
    </div>
</div>

<?php get_footer(); ?>