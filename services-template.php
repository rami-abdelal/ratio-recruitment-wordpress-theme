<?php

/**
 * Template Name: Services
 *
 * @package WordPress
 * @subpackage Eire Workforce Solutions
 * @since Eire Workforce Solutions 1.0
 */


get_header(); ?>

<div id="main" class="services">
   
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       <header class="title">

            <h1 class="full"><?php the_title(); ?>
            <span class="icon-services"></span>
            </h1>

       </header>
    <div class="full" style="display: none">
    
     <div class="material-full material">
         
         <?php the_content(); ?>
         
     </div>
        
    </div>
     
      <div class="services-links large dark">
          
          <div class="full">
              
              <h2 class="left-reveal"><?php the_field('services_links_section_title'); ?></h2>
              <p class="left-reveal"><?php the_field('services_links_section_sentence'); ?></p>
              
              <span class="icon-link right-reveal"></span>
              
          </div>          
          
      </div>
       
       <div class="wide-article light">
           
           <div class="full">
               
                <h4 class="left-reveal"><?php the_field('services_links_article_title'); ?></h4>
                <p class="left-reveal"><?php the_field('services_links_article_text'); ?></p>

                <div class="animation-canvas checklist">
                    <div id="checkmark-1" class="checkmark">
                        <div class="short"></div>
                        <div class="long"></div>
                    </div>
                    <div id="checkmark-2" class="checkmark">
                        <div class="short"></div>
                        <div class="long"></div>
                    </div>
                    <div id="checkmark-3" class="checkmark">
                        <div class="short"></div>
                        <div class="long"></div>
                    </div>
                </div>
               
           </div>
           
       </div>
       
       <div class="services-effective gradient">
           
           <div class="full">
               
               <span class="icon-clients fade-reveal"></span>
               
               <h2 class="left-reveal"><?php the_field('services_fast_and_effective_title'); ?></h2>
               <h3 class="left-reveal"><?php the_field('services_fast_and_effective_subtitle'); ?></h3>
               
           </div>
           
       </div>
       
       <div class="wide-article light">
           
           <div class="full">
               
               <h4 class="left-reveal"><?php the_field('services_fast_and_effective_article_title'); ?></h4>
               <p class="left-reveal"><?php the_field('services_fast_and_effective_article_text'); ?></p>
               
           </div>
           
       </div>
       
       <div class="services-admin dark large" style="background-image:url(<?php the_field('services_administration_image'); ?>);">
          
           <div class="screen">
               
               <div class="full">

                   <h2 class="left-reveal"><?php the_field('services_administration_title'); ?></h2>
                   <h3 class="left-reveal"><?php the_field('services_administration_subtitle'); ?></h3>

               </div>
               
           </div>
           
       </div>
       
       <div class="wide-article light">
           
           <div class="full">
               
               <h4 class="left-reveal"><?php the_field('services_administration_article_title'); ?></h4>
               <p class="left-reveal"><?php the_field('services_administration_article_text'); ?></p>
               
           </div>
           
       </div>

       <div class="services-clients-advert gradient large">
           
           <div class="full">
               
               <h2 class="left-reveal"><?php the_field('services_clients_advert_title'); ?></h2>
               <h3 class="left-reveal"><?php the_field('services_client_advert_subtitle'); ?></h3>
               
               <a class="button bottom-reveal" href="<?php the_field('services_clients_advert_button_page_link'); ?>">
                   
                   <?php the_field('services_clients_advert_button_text'); ?>
                   
               </a>
               
               <span class="icon-clients fade-reveal"></span>
               
           </div>
           
       </div>
   
    <?php endwhile; endif; ?>
        
</div>

<?php get_footer(); ?>